#include <stdio.h>
#include <stdlib.h>

#define DEFAULT 5
int size = DEFAULT;

struct stack {
	int arr[DEFAULT];
	int top;
};

void init_stk(struct stack *st)
{
	st->top = -1;
}

void push(struct stack *st, int num)
{
	if(st->top == size - 1)
	{
		printf("Stack is full.");
		return;
	}

	st->top++;
	st->arr[st->top] = num;
}


int pop(struct stack *st)
{
	int num;

	if(st->top == -1)
	{
		printf("Stack is empty.");
		return 0;
	}

	num = st->arr[st->top];
	st->top--;
	
	return num;
}


/* For displaying the stack */
void display(struct stack *st)
{
	int i;

	printf("\nThe stack is: ");

	for(i = st->top; i >= 0; i--)
	{
		printf("\n|%5d	|\n", st->arr[i]);
	}
}

int main(int argc, char *argv[])
{
	int element, opt;
	struct stack ptr;

	init_stk(&ptr);

	push(&ptr, 3);

	display(&ptr);

	push(&ptr, 5);

	display(&ptr);
	
	pop(&ptr);

	display(&ptr);
	
	return 0;

}
