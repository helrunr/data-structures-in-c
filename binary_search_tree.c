#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct _tree {
    void *data;
    struct _tree *left;
    struct _tree *right;
} TreeNode;

typedef struct _member {
    char name[32];
    unsigned char id;
} Member;


typedef int(*COMPARE) (void*, void*);
typedef int(*DISPLAY) (void*);

int compareMember(Member *e1, Member *e2) {
    return strcmp(e1->name, e2->name);
}

int displayMember(Member *member) {
    printf("%s\t%d\n", member->name, member->id);
}

void insertNode(TreeNode **realRoot, COMPARE compare, void* data) {
    TreeNode *node = (TreeNode*) malloc(sizeof(TreeNode));

    node->data = data;
    node->left = NULL;
    node->right = NULL;

    TreeNode *root = *realRoot;

    /* Check if the tree is empty.*/
    if (root == NULL)
    {
        /* If so, assign a new node to the root and return */
        *realRoot = node;
        return;
    }

    while (1)
    {
        /* On each iteration the new node and current parent node are compared */
        if (compare((root)->data, data) > 0)
        {
            /*
             * Based on the comparison the local root pointer will be reassigned
             * to either the left or right child.
             *
             * This root pointer points to the current node in the tree.
             *
             * If either child is NULL, then the ndoe is added as a child and the
             * loop terminates.
             */
            if (root->left != NULL)
            {
                root = root->left;
            } else {
                root->left = node;
                break;
            }
        } else {
            if (root->right != NULL)
            {
                root = root->right;
            } else {
                root->right = node;
                break;
            }
        }
    }
}

/*                                    *
 * Define ordering of node traversal. *
 *                                    */

/* Left Child, Parent, Right Child */
void inOrder(TreeNode *root, DISPLAY display) {
    if (root != NULL)
    {
        inOrder(root->left, display);
        display(root->data);
        inOrder(root->right, display);
    }

}

/* Left Child, Right Child, Parent */
void postOrder(TreeNode *root, DISPLAY display) {
    if (root != NULL)
    {
        postOrder(root->left, display);
        postOrder(root->right, display);
        display(root->data);
    }
}

/* Parent, Left Child, Right Child */
void preOrder(TreeNode *root, DISPLAY display) {
    if (root != NULL)
    {
        display(root->data);
        preOrder(root->left, display);
        preOrder(root->right, display);
    }
}
