#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct _members {
    char name[32];
    unsigned char id;
} Member;

typedef struct _node {
    void *data;
    struct _node *next;
} Node;

typedef struct _linkedList {
   Node *head;
   Node *tail;
   Node *current;
} LinkedList;

/*
 * Before the linked list can be used it needs to be initialized.
 * This function performs this initialization.
 */
void initLL(LinkedList *list) {
    list->head = NULL;
    list->tail = NULL;
    list->current = NULL;
}

/*
 * Memory is first allocated for the node and the data passed to the function
 * is assigned to the structures data field. By passing the data as a pointer to
 * void, the linked list is able to hold any type of data the user wants to use.
 *
 * There is a check to see whether the linked list is empty. If so, assign the tail
 * pointer to the node and assign NULL to the nodes next field.
 *
 * If not, the node's next pointer is assigned the list's head.
 */
void addHead(LinkedList *list, void *data) {
    Node *node = (Node *) malloc(sizeof(Node));
    node->data = data;

    if (list->head == NULL)
    {
        list->tail = node;
        node->next = NULL;
    } else {
        node->next = list->head;
    }

    list->head = node;
}

/*
 * Adding a tail node.
 *
 * The node after tail will always be NULL as the tail node comes at the end
 * of the linked list.
 */
void addTail(LinkedList *list, void *data) {
    Node *node = (Node *) malloc(sizeof(Node));
    node->data = data;
    node->next = NULL;

    if (list->head == NULL)
    {
        list->head = node;
    } else {
        list->tail = node;
    }

    list->tail = node;
}

int compareMember(Member *m1, Member *m2) {
    return strcmp(m1->name, m2->name);
}

int showMember(Member *member) {
    printf("%s %d\n", member->name, member->id);
}

typedef void(*SHOW) (void*);
typedef int(*COMPARE) (void*, void*);

/*
 * The variable node initially points to the lists head and traverses the list until either
 * a match is found or the linked list's end is encountered. The compare function is called
 * to determine if a match is found.
 */
Node *getNode (LinkedList *list, COMPARE compare, void* data) {
    Node *node = list->head;

    while (node != NULL)
    {
        if (compare(node->data, data) == 0)
        {
            return node;
        }

        node = node->next;
    }

    printf("\n%s\n", "No node found.");

}

void delete (LinkedList *list, Node *node) {

    /* check to see if the node is the head */
    if (node == list->head) {
        /* If so, check to see if the next node is NULL */
        if (list->head->next == NULL) {
            /* If so, this means you are dealing with the tail */
            list->head = list->tail = NULL;
        } else {
            /* If not, set the next node as the head */
            list->head = list->head->next;
        }
        /* The node you are dealing with is not the head */
    } else {
        /* Assign the head to a temporary variable */
        Node *tmp = list->head;

        /*
         * Traverse the list.
         *
         * If tmp is not NULL and the next node does not equal the node being
         * deleted continue traversing until you find the node you are looking for.
         */
        while (tmp != NULL && tmp->next != node)
        {
            tmp = tmp->next;
        }

        if (tmp != NULL)
        {
            tmp->next = node->next;
        }
    }

    free(node);

}

void displayLinkedList (LinkedList *list, SHOW show) {
    Node *current = list->head;

    while (current != NULL)
    {
        show(current->data);
        current = current->next;
    }
}

