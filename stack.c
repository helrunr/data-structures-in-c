#include <stdio.h>
#include <stdlib.h>

#define DEFAULT 5
int top, status;

void push(int stack[], int item)
{
	if (top == (DEFAULT-1))
	{
		status = 0;
	} else {
		status = 1;
		++top;
		stack[top] = item;
	}
}

int pop(int stack[])
{
	int ret;

	if(top == -1)
	{
		ret = 0;
		status = 0;
	} else {
		status = 1;
		ret = stack[top];
		--top;
	}
	
	return ret;
}

/* For displaying the stack */
void display(int stack[])
{
	int i;
	printf ("\nThe stack is: ");

	if (top == -1)
	{
		printf("empty");
	} else {
		for (i = top; i >= 0; --i)
		{
			printf("\n|%5d	|\n", stack[i]);

		}
	}
	
	printf("\n");
}

int main(int argc, char *argv[])
{

	int stack[DEFAULT], item, ch;
	top = -1;

	push(stack, 3);

	display(stack);

	push(stack, 5);

	display(stack);
	
	pop(stack);

	display(stack);
	
	return 0;

}
