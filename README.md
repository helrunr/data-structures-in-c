## Data Structures In C ##

Implementation of various data structures in C. Compiled using gcc on Debian 9.

**stack.c**
* A LIFO structure that is an ordered list of elements of the same type.

**pointer_stack.c**
* An implementation of the stack data structure using pointers.

**linked_list.c**
* An implementation of a single-linked list data structure. A series of nodes
connected with links.

**binary_search_tree.c**
* An implementation of a binary search tree data structure. An inverted tree where
a root node represents the data structure's starting element. 
---

### Linked List ###
Usage instructions for the linked list program.

**Adding Members and displaying them**
```C
    /* Instantiate the linked list */
    LinkedList linkedList;

    /* Populate the data elements for the nodes. */
    Member *brandon = (Member *) malloc(sizeof(Member));
    strcpy(brandon->name, "brandon");
    brandon->id = 19;

    Member *gonzales = (Member *) malloc(sizeof(Member));
    strcpy(gonzales->name, "gonzales");
    gonzales->id = 20;

    /* Initialize the linked list */
    initLL(&linkedList);

    /* Add defined nodes to the linked list */
    addHead(&linkedList, brandon);
    addHead(&linkedList, gonzales);

    displayLinkedList(&linkedList, (SHOW)showMember);
```

**Getting data of a particular node**
```C
    /* getNode returns the node of the member. This is of type Node. */
    Node *comp = getNode(&linkedList, (COMPARE)compareMember, "brandon");

    /* The emp pointer gets the data associated with the node comp. This is of type Member. */
    Member *mem = comp->data;

    /* The data of the member is now available in mem, this is placed into the variable, store. */
    unsigned char store = mem->id;

    /* Print the member data. */
    printf("\n%d\n", store);
```

---

### Binary Search Tree ###
Usage instructions for the binary search tree program.

**Adding and displaying members**
```C
/* Instantiate a tree */
TreeNode *tree = NULL;

/* Define members */
Member *brandon = (Member *) malloc(sizeof(Member));
strcpy(brandon->name, "brandon");
brandon->id = 01;

Member *gonzales = (Member *) malloc(sizeof(Member));
strcpy(gonzales->name, "gonzales");
gonzales->id = 02;

Member *user = (Member *) malloc(sizeof(Member));
strcpy(user->name, "user");
user->id = 03;

/* Add members to the tree */
insertNode(&tree, (COMPARE) compareMember, brandon);
insertNode(&tree, (COMPARE) compareMember, gonzales);
insertNode(&tree, (COMPARE) compareMember, user);

/* Display functions */
preOrder(tree, (DISPLAY) displayMember);
inOrder(tree, (DISPLAY) displayMember);
postOrder(tree, (DISPLAY) displayMember);
```
